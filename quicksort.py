import time, random
from sys import stdout as o

def timing(f):
    """Decorator for timing functions"""
    def wrap(*args):
        time1 = time.clock()
        ret = f(*args)
        time2 = time.clock()
        print ("%0.3f s" % (time2-time1))
        return ret
    return wrap

@timing
def quicksort(array, fixed):
    quicksort0(array, 0, len(array) - 1, fixed)
     
def quicksort0(array, start, stop, fixed):
    if stop - start > 0:
        pivot, left, right = array[start if fixed else random.randint(start,stop)], start, stop
        while left <= right:
            while array[left] < pivot:
                left += 1
            while array[right] > pivot:
                right -= 1
            if left <= right:
                array[left], array[right] = array[right], array[left]
                left += 1
                right -= 1
            quicksort0(array, start, right, fixed)
            quicksort0(array, left, stop, fixed)

def main():
    n = 256

    # Generate arrays
    C1 = [random.randint(0, 1000) for i in range(n)]
    C2 = C1[:]
    
    print("Quicksort with random pivot")
    print(C2)
    o.write("*** Time : ");
    quicksort(C2, False)
    print(C2)
    
    print("\nQuicksort with fixed pivot")
    print(C1)
    o.write("*** Time : ");
    quicksort(C1, True)
    print(C1)

    
if __name__ == "__main__":
    main()
